const PERSONS = [
  { id: 1, firstName: "Mateusz" },
  { id: 2, firstName: "Cezary" },
  { id: 3, firstName: "Mikołaj" },
  { id: 3, firstName: "Bartek" }
];

function getFirstNameById(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const person = PERSONS.find((person) => person.id === id);

      if (person) resolve(person.firstName);
      else reject(new Error("Person not found"));
    }, 1000);
  });
}

async function main() {
  const firstName1 = await getFirstNameById(1);

  console.log(firstName1);

  try {
    const firstName2 = await getFirstNameById(6)

    console.log(firstName2);
  } catch (err) {
    console.log("Error:", err.message)
  }
}

main();

