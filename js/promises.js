const PERSONS = [
  { id: 1, firstName: "Mateusz" },
  { id: 2, firstName: "Cezary" },
  { id: 3, firstName: "Mikołaj" },
  { id: 3, firstName: "Bartek" }
];

function getFirstNameById(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const person = PERSONS.find((person) => person.id === id);

      if (person) resolve(person.firstName);
      else reject(new Error("Person not found"));
    }, 1000); // 1s
  });
}

function main() {
  getFirstNameById(1)
    .then((firstName) => console.log(firstName))
    .then(() => getFirstNameById(6))
    .then((firstName) => console.log(firstName))
    .catch((err) => console.log("Error:", err.message));
}

main();

