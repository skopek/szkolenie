const path = require('path');

const common = (isServer) => ({
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: isServer ? { node: 'current' } : { ie: 11 } }],
              '@babel/preset-react',
              '@babel/preset-typescript'
            ]
          }
        }
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx']
  },
});

const clientConfig = {
  ...common(false),

  entry: './src/client.tsx',

  output: {
    filename: 'client.bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};

const serverConfig = {
  ...common(true),

  entry: './src/server.tsx',

  target: 'node',

  node: {
    __dirname: false
  },

  output: {
    filename: 'server.bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};

module.exports = [clientConfig, serverConfig];