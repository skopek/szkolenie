import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './components/App';

ReactDOM.render((
  <BrowserRouter>
    <App initialText="rendered on the client side!" />
  </BrowserRouter>), document.getElementById('app')
);