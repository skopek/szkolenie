import express from 'express';
import { resolve } from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import App from './components/App';
import Html from './components/Html';
import { StaticRouter } from 'react-router-dom';

const app = express();

app.get('*.*', express.static(__dirname, {
  maxAge: '1y'
}));

app.get('*', async (req, res, next) => {
  const scripts = ['client.bundle.js'];
  const context = {};

  const appMarkup = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App initialText="rendered on the server" />
    </StaticRouter>
  );
  const html = ReactDOMServer.renderToStaticMarkup(
    <Html children={appMarkup} scripts={scripts} />
  );

  res.send(`<!doctype html>${html}`);
});

app.listen(3000, () => console.log('Listening on localhost:3000'));