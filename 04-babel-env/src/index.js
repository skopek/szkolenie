const x = [1, 2, 3].map(n => n ** 2)

console.log(...x); // ...x => x[0], x[1], x[2]