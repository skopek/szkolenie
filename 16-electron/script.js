const content = document.getElementById('content');

function createCard(city) {
  const cardContainer = document.createElement('div');
  const card = document.createElement('div');
  const name = document.createElement('h2');
  const temp = document.createElement('div');
  const pressure = document.createElement('div');
  const humidity = document.createElement('div');

  cardContainer.classList.add('card-container');
  card.classList.add('card');
  name.classList.add('card__title');
  temp.classList.add('item');
  pressure.classList.add('item');
  humidity.classList.add('item');

  name.innerHTML = city.name;
  temp.innerHTML = "<strong>Temperatura: </strong>" + city.main.temp + " &#8451;";
  pressure.innerHTML = "<strong>Ciśnienie: </strong>" + city.main.pressure + " Pa";
  humidity.innerHTML = "<strong>Wilgotność: </strong>" + city.main.humidity;

  card.appendChild(name);
  card.appendChild(temp);
  card.appendChild(pressure);
  card.appendChild(humidity);

  cardContainer.appendChild(card);

  content.appendChild(cardContainer);
}

function loadData(data) {
  data.list.forEach(createCard);
}

fetch('http://api.openweathermap.org/data/2.5/group?id=3102014,756135,3081368,3094802,7530858,3093133,3099434,3083829&appid=90db1744bdeb0ba92a92e60b284198b8&units=metric')
  .then(res => res.json())
  .then(data => loadData(data));


// Rejestracja Service Worker'a
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js').then(function (r) {
    console.log('ServiceWorker zarejestrowany.')
  }).catch(function (e) {
    console.log('Ups! Błąd przy rejestracji ServiceWorkera! ' + e)
  });
}
