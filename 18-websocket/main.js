const mongoose = require('mongoose');
const express = require('express');
const http = require('http');
const WebSocket = require('ws');

// Express

const app = express();
const port = 3000;

const server = http.createServer(app);

// Websocket

function sendMessage(msg) {
  const instance = new MessageModel({ ...msg, date: new Date() });

  instance.save((err, data) => {
    if (!err) {
      wss.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) client.send(JSON.stringify(data));
      });
    }
  });
}

const wss = new WebSocket.Server({ server, path: '/chat' });

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    try {
      sendMessage(JSON.parse(message));
    } catch (e) { }
  });
});

// MongoDB

mongoose.connect('mongodb://localhost/chat', { useNewUrlParser: true });

const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  author: { type: String, required: true },
  body: { type: String, required: true },
  date: { type: Date, required: true }
});

const MessageModel = mongoose.model('Message', MessageSchema);

// Routing

app.get('/messages', (req, res) => {
  MessageModel.find({}, (err, data) => {
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true, data });
  });
});

// Uruchomienie serwera HTTP

server.listen(port, () => console.log(`Serwer uruchomiony na porcie ${port}!`));
