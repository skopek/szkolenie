import React from 'react';
import { Todo } from './Todo';

interface TodoListProps {
  items: Todo[];
}

export default class TodoList extends React.Component<TodoListProps> {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.text}</li>
        ))}
      </ul>
    );
  }
}