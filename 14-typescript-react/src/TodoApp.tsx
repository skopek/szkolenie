import React from 'react';
import { Todo } from './Todo';
import TodoList from './TodoList';

interface TodoAppState {
  items: Todo[];
  text: string;
}

export default class TodoApp extends React.Component<{}, TodoAppState> {
  constructor(props: {}) {
    super(props);
    this.state = { items: [], text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} />
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="new-todo">
            What needs to be done?
          </label>
          <input
            id="new-todo"
            onChange={this.handleChange}
            value={this.state.text}
          />
          <button>
            Add #{this.state.items.length + 1}
          </button>
        </form>
      </div>
    );
  }

  handleChange(e: any) {
    this.setState({ text: e.target.value });
  }

  handleSubmit(e: any) {
    e.preventDefault();
    if (!this.state.text.length) {
      return;
    }
    const newItem: Todo = {
      text: this.state.text,
      id: Date.now()
    };
    this.setState(state => ({
      items: state.items.concat(newItem),
      text: ''
    }));
  }
}