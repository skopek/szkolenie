import React from 'react';

class TaskForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      task: {
        title: '',
        body: ''
      }
    };
  }

  handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      task: {
        ...this.state.task,
        [name]: value
      }
    });
  }

  handleSubmit() {
    const task = this.state.task;
    const onChange = this.props.onChange;

    fetch('http://localhost:3000/task', {
      method: 'put',
      body: JSON.stringify(task),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(res => res.json())
      .then(() => {
        this.setState({
          task: {
            title: '',
            body: ''
          }
        });
        onChange();
      });
  }

  render() {
    const { task } = this.state;
    const { title, body } = task;

    return (
      <div className="task-form">
        <form>
          <div>
            <input name="title" value={title} onChange={(e) => this.handleChange(e)} />
          </div>

          <div>
            <textarea name="body" value={body} onChange={(e) => this.handleChange(e)}></textarea>
          </div>

          <button type="button" onClick={() => this.handleSubmit()}>Dodaj</button>
        </form>
      </div>
    );
  }
}

export default TaskForm;
