import React from 'react';

function Task({ title, body, date }) {
  return (
    <div className="task-container">
      <div className="task">
        <h2 className="task-title">{title}</h2>
        <div className="task-date">{date}</div>
        <p className="task-body">{body}</p>
      </div>
    </div>
  );
}

export default Task;
