import React from 'react';
import './App.css';
import Task from './Task';
import TaskForm from './TaskForm';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = { tasks: [] };
  }

  fetchTasks() {
    fetch('http://localhost:3000/tasks')
      .then(res => res.json())
      .then(({ data }) => this.setState({ tasks: data }));
  }

  componentDidMount() {
    this.fetchTasks();
  }

  render() {
    const { tasks } = this.state;

    return (
      <div>
        {tasks.map((task) => <Task key={task.id} {...task} />)}

        <TaskForm onChange={() => this.fetchTasks()} />
      </div>
    );
  }
}

export default App;
