class Timer extends React.Component {
  constructor(props) {

    console.log(props);
    super(props);
    this.state = { seconds: 0 };
    this.test = 133;
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        Seconds: {this.state.seconds}
      </div>
    );
  }
}

ReactDOM.render(
  <Timer test="123" />,
  document.getElementById('timer-example')
);