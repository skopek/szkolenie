import Data from './data.xml';
import Logo from './logo.png';
import './style.css';
import { component } from './util';

const comp = component();

comp.classList.add('hello');

const myLogo = new Image();
myLogo.src = Logo;


comp.appendChild(myLogo);

document.body.appendChild(comp);

console.log(Data);
