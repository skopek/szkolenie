const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

// Express

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(cors());

// MongoDB

mongoose.connect('mongodb://localhost/my_database', { useNewUrlParser: true });

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  title: { type: String, required: true },
  body: { type: String, required: true },
  date: { type: Date, required: true }
});

const TaskModel = mongoose.model('Task', TaskSchema);

// Routing

app.get('/', (req, res) => res.send('Hello World!'));

app.put('/task', (req, res) => {
  console.log(req.body);

  const instance = new TaskModel({ ...req.body, date: new Date() });

  instance.save((err) => {
    console.log(err);
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true });
  });
});

app.get('/task', (req, res) => {
  const id = req.query.id;

  if (!id) {
    res.status(404).end();
    return;
  }

  TaskModel.findById(id, (err, data) => {
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true, data });
  });
});

app.get('/tasks', (req, res) => {
  TaskModel.find({}, (err, data) => {
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true, data });
  });
});

app.post('/task', (req, res) => {
  const id = req.query.id;

  if (!id) {
    res.status(404).end();
    return;
  }

  TaskModel.findByIdAndUpdate(id, req.body, (err) => {
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true });
  });
});

app.delete('/task', (req, res) => {
  const id = req.query.id;

  if (!id) {
    res.status(404).end();
    return;
  }

  TaskModel.findByIdAndRemove(id, (err) => {
    if (err) res.status(500).json({ ok: false });
    else res.json({ ok: true });
  });
});

// Uruchomienie serwera HTTP

app.listen(port, () => console.log(`Serwer uruchomiony na porcie ${port}!`));
