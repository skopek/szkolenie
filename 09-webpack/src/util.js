import _ from 'lodash';

export function component() {
  const element = document.createElement('div');

  element.classList.add('hello');

  element.innerHTML = _.join(['Hello', 'webpack'], ' ');

  return element;
}
