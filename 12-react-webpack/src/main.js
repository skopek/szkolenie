import React from 'react';
import ReactDOM from 'react-dom';
import HelloMessage from './hello';

ReactDOM.render(
  <HelloMessage name="Mateusz" />,
  document.getElementById('app')
);