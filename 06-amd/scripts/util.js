function changeContent(element, text) {
  element.innerText = text;
}

define(function () {
  return { changeContent: changeContent };
});