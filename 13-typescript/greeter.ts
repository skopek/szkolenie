interface Person {
  firstName: string;
  lastName: string;
}

const greeter = (person: Person) => {
  return "Hello, " + person.firstName + " " + person.lastName;
}

let user = {
  firstName: 'Mateusz',
  lastName: 'Skopowski',
};

document.body.textContent = greeter(user);