const greeter = (person) => {
  return "Hello, " + person.firstName + " " + person.lastName;
}

let user = {
  firstName: 'Mateusz',
  lastName: 'Skopowski'
};

document.body.textContent = greeter(user);