import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  messages = [
    { body: "Hello" },
    { body: "Hello" },
    { body: "Hello" },
  ];

  message: string = "";

  constructor() { }

  sendMessage() {
    console.log(this.message);
  }
}
